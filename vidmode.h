/*
    vidmode.h - abstract handling of vidmode changing for gvidm
    Copyright (C) 2001-2004 Matthew Mueller <donut AT dakotacom DOT net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef __VIDMODE_H__
#define __VIDMODE_H__

#include "gvid.h"

struct VidMode {
	bool iscurmode;
	int width, height, refresh;
	string info;
	void *data;

	VidMode(bool cur, int w, int h, int r, const string &i, void *d)
		: iscurmode(cur), width(w), height(h), refresh(r), info(i), data(d)
		{ };
	string str() const;
	string pretty_str() const;
};

typedef vector<VidMode*> mode_list_t;
typedef vector<mode_list_t> screen_mode_list_t;


int parse_mode(int &w, int &h, int &r, const string &arg);
int parse_arg(screen_list_t &screens, int &w, int &h, int &r, const string &arg, int num_screens, int cur_screen);
int find_mode(mode_list_t &modes, int screen, int w, int h, int r, const screen_mode_list_t &available_modes);
int parse_arg_mode(screen_mode_list_t &arg_modes, const char *arg, const screen_mode_list_t &available_modes, int cur_screen);
int parse_arg_modes(screen_mode_list_t &arg_modes, char const*const*arg0, char const*const*argend, const screen_mode_list_t &available_modes, int cur_screen);


class AbstractVidModeHandler : public Handler {
	protected:
		int cur_screen;
		screen_mode_list_t available_modes;
		int list(const screen_list_t &screens, bool onlycur) const;

	public:
		AbstractVidModeHandler();
		virtual ~AbstractVidModeHandler();

		virtual int set(const char *const* argbegin, const char *const* argend);
		virtual int list(const char *const* argbegin, const char *const* argend) const;
		virtual int list_current(const char *const* argbegin, const char *const* argend) const;
};

#ifdef HAVE_LIBXXF86VM
class XVidModeHandler : public AbstractVidModeHandler {
	public:
		XVidModeHandler();
		virtual ~XVidModeHandler();

		virtual int doevent(gpointer data);
};
#endif

#ifdef HAVE_LIBXRANDR
class XRandRHandler : public AbstractVidModeHandler {
	private:
		XRRScreenConfiguration ** screenconfigs;
		int num_screens;
	public:
		XRandRHandler();
		virtual ~XRandRHandler();

		virtual int doevent(gpointer data);
};
#endif

#endif
