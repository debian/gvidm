/*
    gvidm - X11 video mode changer with a minimal interface
    Copyright (C) 2001-2004 Matthew Mueller <donut AT dakotacom DOT net>
    based on gvid:
    Copyright (C) 1999-2001 Keith Vanderline <kvand@mit.edu>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef __GVID_H__
#define __GVID_H__

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

extern "C" {
#include <X11/Xlib.h>
#ifdef HAVE_LIBXXF86VM
#include <X11/extensions/xf86vmode.h>
#endif
#ifdef HAVE_LIBXINERAMA
#include <X11/extensions/Xinerama.h>
#endif
#ifdef HAVE_XF86PARSER_H
#include <xf86Parser.h>
#else
#define XF86CONF_DBLSCAN 0
#define XF86CONF_INTERLACE 0
#endif
#ifdef HAVE_LIBXRANDR
#include <X11/extensions/Xrandr.h>
#endif
}
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#define M_VERSION "0.8"
#define VERSION "0.3"
#define PACKAGE "gvidm"


using namespace std;

#include <string>
#include <vector>
typedef vector<int> screen_list_t;


extern Display *display;

int get_screen_count(void);
int get_current_screen(void);
int parse_int(int &i, const string &arg);
int parse_screen(screen_list_t &screens, const string &arg, int num_screens, int cur_screen);
int parse_arg_screen(screen_list_t &screens, const char * arg, int num_screens, int cur_screen);
int parse_arg_screens(screen_list_t &screens, char const*const*arg0, char const*const*argend, const char * def, int num_screens, int cur_screen);
int round(float f);

GtkWidget *create_root_menu();
GtkWidget *create_menuitem_with_accel(const char *label_str, GtkWidget *menu_ptr);
void add_menuitem_with_accel(const char *label_str, void *data, GtkWidget *menu_ptr);

inline char num2char36(int i) {
	if (i<10) return '0'+i;
	return 'a'+i-10;
}


class Handler {
	public:
		virtual int set(const char *const* argbegin, const char *const* argend)=0;
		virtual int list(const char *const* argbegin, const char *const* argend) const=0;
		virtual int list_current(const char *const* argbegin, const char *const* argend) const=0;
		virtual int doevent(gpointer data)=0;
};


#endif
