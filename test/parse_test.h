#include <cxxtest/TestSuite.h>
#include "../gvid.h"

class TestParseInt : public CxxTest::TestSuite
{                                            
	public:
		int i,r;
		void setUp() { i=-123; r=-1234; }

		void test_parse_int_error()
		{
			r=parse_int(i, "foo");
			TS_ASSERT_DIFFERS(r, 0);
		}
		void test_parse_int_partialerror()
		{
			r=parse_int(i, "234foo");
			TS_ASSERT_DIFFERS(r, 0);
		}
		void test_parse_int()
		{
			r=parse_int(i, "42");
			TS_ASSERT_EQUALS(r, 0);
			TS_ASSERT_EQUALS(i, 42);
			r=parse_int(i, "-983");
			TS_ASSERT_EQUALS(r, 0);
			TS_ASSERT_EQUALS(i, -983);
		}
};

class TestParseScreen : public CxxTest::TestSuite
{                                            
	public:
		screen_list_t screens;
		void setUp() { screens.clear(); }
		void test_parse_screen() {
			TS_ASSERT_EQUALS(parse_screen(screens, "0", 1, 0), 0);
			TS_ASSERT_EQUALS(screens.size(), 1);
			TS_ASSERT_EQUALS(screens.front(), 0);
		}
		void test_parse_screen2() {
			TS_ASSERT_EQUALS(parse_screen(screens, "3", 10, 0), 0);
			TS_ASSERT_EQUALS(parse_screen(screens, "5", 10, 1), 0);
			TS_ASSERT_EQUALS(parse_screen(screens, "9", 10, 9), 0);
			TS_ASSERT_EQUALS(screens.size(), 3);
			TS_ASSERT_EQUALS(screens.front(), 3);
			TS_ASSERT_EQUALS(screens[1], 5);
			TS_ASSERT_EQUALS(screens[2], 9);
		}
		void test_parse_screen_cur() {
			TS_ASSERT_EQUALS(parse_screen(screens, "cur", 4, 0), 0);
			TS_ASSERT_EQUALS(screens.size(), 1);
			TS_ASSERT_EQUALS(screens.front(), 0);
		}
		void test_parse_screen_cur2() {
			TS_ASSERT_EQUALS(parse_screen(screens, "cur", 4, 3), 0);
			TS_ASSERT_EQUALS(screens.size(), 1);
			TS_ASSERT_EQUALS(screens.front(), 3);
		}
		void test_parse_screen_all() {
			TS_ASSERT_EQUALS(parse_screen(screens, "all", 4, 0), 0);
			TS_ASSERT_EQUALS(screens.size(), 4);
			TS_ASSERT_EQUALS(screens.front(), 0);
			TS_ASSERT_EQUALS(screens[1], 1);
			TS_ASSERT_EQUALS(screens[2], 2);
			TS_ASSERT_EQUALS(screens[3], 3);
		}
		void test_parse_screen_error() {
			TS_ASSERT_DIFFERS(parse_screen(screens, "0foo", 1, 0), 0);
			TS_ASSERT_EQUALS(screens.size(), 0);
		}

		void test_parse_arg_screens() {
			char *args[]={"5","3","foo","8"};
			TS_ASSERT_EQUALS(parse_arg_screens(screens, args, args+4, "all", 10, 0), 1);
			TS_ASSERT_EQUALS(screens.size(), 3);
			TS_ASSERT_EQUALS(screens.front(), 5);
			TS_ASSERT_EQUALS(screens[1], 3);
			TS_ASSERT_EQUALS(screens[2], 8);
		}
		void test_parse_arg_screens_def() {
			TS_ASSERT_EQUALS(parse_arg_screens(screens, NULL, NULL, "all", 4, 0), 0);
			TS_ASSERT_EQUALS(screens.size(), 4);
			TS_ASSERT_EQUALS(screens.front(), 0);
			TS_ASSERT_EQUALS(screens[1], 1);
			TS_ASSERT_EQUALS(screens[2], 2);
			TS_ASSERT_EQUALS(screens[3], 3);
		}
		void test_parse_arg_screens_errnodef() {
			char *args[]={"5aa","jj3","foo","3-8"};
			TS_ASSERT_EQUALS(parse_arg_screens(screens, args, args+4, "all", 10, 7), 4);
			TS_ASSERT_EQUALS(screens.size(), 0);
		}
};
