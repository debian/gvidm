#ifndef __GVIDM__IOS_H__
#define __GVIDM__IOS_H__

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_IOS
#include <ios>
#else
#include <iostream>
#endif

#if !HAVE_DECL_STD__FIXED
inline std::ios& fixed(std::ios &s) {
	s.setf(std::ios::fixed, std::ios::floatfield);
	return s;
}
#endif

#if !HAVE_DECL_STD__LEFT
inline std::ios& left(std::ios &s) {
	s.setf(std::ios::left, std::ios::adjustfield);
	return s;
}
#endif

#endif
