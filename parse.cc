/*
    parse.cc - general/screen parsing functions
    Copyright (C) 2001-2004 Matthew Mueller <donut AT dakotacom DOT net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "gvid.h"


int parse_int(int &i, const string &arg) {
	char *endptr;
	i = strtol(arg.c_str(), &endptr, 10);
	if (arg.empty() || *endptr!='\0') return 1;
	return 0;
}

int parse_screen(screen_list_t &screens, const string &arg, int num_screens, int cur_screen) {
	if (arg == "all") {
		for (int screen=0; screen < num_screens; ++screen)
			screens.push_back(screen);
	} else if (arg == "cur") {
		screens.push_back(cur_screen);
	} else {
		int screen;
		if (parse_int(screen, arg)) return 1;
		if (screen<0 || screen>=num_screens) return 1;
		screens.push_back(screen);
	}
	return 0;
}

int parse_arg_screen(screen_list_t &screens, const char * arg, int num_screens, int cur_screen) {
	if (parse_screen(screens, arg, num_screens, cur_screen)) {
		fprintf(stderr, "invalid screen \"%s\"\n", arg);
		return 1;
	}
	return 0;
}

int parse_arg_screens(screen_list_t &screens, char const*const*arg0, char const*const*argend, const char * def, int num_screens, int cur_screen) {
	int errs = 0;
	if (def && arg0>=argend) {
		arg0=&def;
		argend=&def+1;
	}
	for (; arg0<argend; ++arg0) {
		errs += parse_arg_screen(screens, *arg0, num_screens, cur_screen);
	}
	return errs;
}
