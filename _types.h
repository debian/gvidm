#ifndef __GVIDM_TYPES_H__
#define __GVIDM_TYPES_H__

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#if HAVE_INTTYPES_H
# include <inttypes.h>
#else
# if HAVE_STDINT_H
#  include <stdint.h>
# endif
#endif

#ifndef HAVE_UINTPTR_T
typedef unsigned int uintptr_t;
#endif

#endif
